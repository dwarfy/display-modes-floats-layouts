### All balloons have float: left;
[Click here](close_all; open_preview floats/game.html#noClear=1&config=red-fl-cn|green-fl-cn|blue-fl-cn|yellow-fl-cn|pink-fl-cn panel=0) to see it or click on "left" next to all the small balloons in the controls.

In this case all the balloons form a line from left to right on the top of the page, they are "floating" from left to right in the order in which they appear in the document and because they are "floating" they don't take up any space, allowing the paragraphs of text underneath to go all they way up at the top of the page and pushing the first part of the text to the left. 

This demonstrate the basic behaviour of left floating elements : 
 
- They "float" on the top left of their containers, except :
  - if there are other floating elements before them, then the go directly on the right of them (You can see this in action here, as the 5 balloons are in a line 1-2-3-4-5 at the top of their container).
  - if there are other block level elements before them, then they float "under" them (If you click on "none" in the controls next to the first balloon you will see this in action, the first balloon will be on a its own line at the top, and the other ones will form a line 2-3-4-5 underneath it, [Click here](close_all; open_preview floats/game.html#noClear=1&config=red-fn-cn|green-fl-cn|blue-fl-cn|yellow-fl-cn|pink-fl-cn panel=0) to see it.)

- They "allow" other elements to overlap with them, but pushing the text content (and other inline elements) of them to the right so they have enough "space".### All balloons have float: left;
[Click here](close_all; open_preview floats/game.html#noClear=1&config=red-fl-cn|green-fl-cn|blue-fl-cn|yellow-fl-cn|pink-fl-cn panel=0) to see it or click on "left" next to all the small balloons in the controls.

In this case all the balloons form a line from left to right on the top of the page, they are "floating" from left to right in the order in which they appear in the document and because they are "floating" they don't take up any space, allowing the paragraphs of text underneath to go all they way up at the top of the page and pushing the first part of the text to the left. 

This demonstrate the basic behaviour of left floating elements : 
 
- They "float" on the top left of their containers, except :
  - if there are other floating elements before them, then the go directly on the right of them (You can see this in action here, as the 5 balloons are in a line 1-2-3-4-5 at the top of their container).
  - if there are other block level elements before them, then they float "under" them (If you click on "none" in the controls next to the first balloon you will see this in action, the first balloon will be on a its own line at the top, and the other ones will form a line 2-3-4-5 underneath it, [Click here](close_all; open_preview floats/game.html#noClear=1&config=red-fn-cn|green-fl-cn|blue-fl-cn|yellow-fl-cn|pink-fl-cn panel=0) to see it.)

- They "allow" other elements to overlap with them, but pushing the text content (and other inline elements) of them to the right so they have enough "space".