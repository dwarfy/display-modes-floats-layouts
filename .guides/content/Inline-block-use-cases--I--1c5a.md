### 1. A gallery.

Have a look at the left panels, we use a serie of spans with display inline-block. The cool thing is that they fill the screen until the end of the line and then go onto the next line. 

If you resize the preview window, you will see that more boxes will fill each line in order to take all the horizontal space.
