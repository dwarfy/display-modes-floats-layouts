# Recap

In the previous unit, we have seen what is the box model, how to use it to put space inside and outside your boxes as well as giving dimensions to your content. We have also seen how to use margins for practical usages like centering your content.

# In this unit

In this unit we will continue exploring the positioning and layout possibilites of html and css. We will see new html elements to structure your content, the different display modes of elements as well as floats and their use.