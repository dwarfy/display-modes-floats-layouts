On the left you will find a little "balloon" playground for floats. What better than a balloon to play with "float"ing stuff.
It's composed of 3 main elements.

1. The big balloons, numbered 1 to 5 are all block-level elements that look like .. balloons.
1. The multiple paragraphs of text underneath, which are used to show the interaction of floating elements with text.
1. Finally the controls at the bottom which will show you and let you change if a balloon is floating or not.


### Initially
None of the balloons are floating. They are stacked on top of each others, with the paragraphs of text underneath. We will see in the next sections a series of example, based on this game to understand the properties of floats.





### All floated right
[Click here](close_all; open_preview "floats/game.html#noClear=1&config=red-fr-cn|green-fr-cn|blue-fr-cn|yellow-fr-cn|pink-fr-cn" panel=0) to see it or click on "right" next to all the small balloons in the controls.

### Some on the left and some on the right
[Click here](close_all; open_preview "floats/game.html#noClear=1&config=red-fl-cn|green-fr-cn|blue-fl-cn|yellow-fr-cn|pink-fl-cn" panel=0) to see it or click on "right" next to all the small balloons in the controls.

### 1st balloon not floated and all the other ones floated left
[Click here](close_all; open_preview "floats/game.html#noClear=1&config=red-fn-cn|green-fl-cn|blue-fl-cn|yellow-fl-cn|pink-fl-cn" panel=0) to see it or click on "none" next to the first balloon and "left" for all the other balloons.