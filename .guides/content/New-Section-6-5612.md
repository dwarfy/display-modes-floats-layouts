### display : inline

Here are some examples of elements who have `display:block` by default, they are called block-level elements : `<span>`, `<a>`, `<b>`, `<i>`, `<img>`

The properties of an inline elements are :

- It will take the width of its content.
- It will NOT begin on a new line by default.
- It may only content text or other inline elements.

## What about the box model ?

Block-level elements fully respect the box model as explained in the previous unit.

On the other end, inline elements do not :

Here are some examples to demonstrate it :