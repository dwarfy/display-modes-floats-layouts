If there are too many floats to fit on a line, the will go onto the next line.

Here we have 15 balloons and have `float:left;` on all of them. As there is not enought space to fit them all on one line, the browser will put as many as he can on the first line and then, go onto the next line to put some more until they are all there.

If you try to resize your browser window and make it less wide, you will see that the browser adapts the layout of the floats and the text in real time!

