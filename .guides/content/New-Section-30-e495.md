### Verifying the box model properties.

As we have done with the inline elements, let's have a look at how inline-block elements behave inside a text when we add box model properties.

[Click here](close_all; open_file display-modes/inline-block1.html panel=0; open_preview display-modes/inline-block1.html panel=1)

As we can see from the examples, all the properties of the box model are respected, while the elements still remain "inline" with the content !