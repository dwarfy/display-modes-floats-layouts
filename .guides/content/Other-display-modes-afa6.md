We have seen the two main display modes, but there are many others.

We will see two more :

```css
display:inline-block;
display:none;
```

## display:none; 

This one is quite easy to understand, and I'm sure you will have figured it out.
When you set `display:none;` on an element it is not displayed on screen anymore, it dissappears.

Beware that it totally disappears as it was never in the html in the first place. So the space that it was taking will be released so that other elements can use this space.


