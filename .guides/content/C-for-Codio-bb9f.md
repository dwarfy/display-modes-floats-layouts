On the left we have the "C" as in the previous challenge. The trick here is to have the blue balloon with `clear:left;` or `clear:both;` so ti will not go up next to the balloons #1 and #2 and again to have the #4 with `clear:left;` or `clear:both;`.

We have played with balloons for a while in order to understand fully how the `float` and `clear` properties work and can be used together to do a combination of layouts.

Nevertheless do not think you can only use floats to play with balloons. In the next section we will see how to use float and clear in order to do a full site layout.