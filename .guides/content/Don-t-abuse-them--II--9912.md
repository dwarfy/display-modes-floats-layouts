### 2) Paragraph and emphasize example

(You can also see it on the left)

```html
<div id="paragraph">
    This is <span class="emphasize">nice</span>
</div>
```

If you want a paragraph, use a `p` and if you want to emphasize something, use `em` or `b`, like this :

```html
<p>
    This is <b>nice</b>
</p>
```

{Check It!|assessment}(test-1318231201)


Earlier we have said that `<div>` is a block level element and that `<span>` is an inline element, let's head to the next section to see more about that.