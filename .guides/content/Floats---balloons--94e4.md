On the left you will find a little playground for floats. What better than a balloon to play with "float"ing elements ?

The playground is composed of 3 main parts : 

1. The big balloons, numbered 1 to 5 are all block-level elements that look like .. balloons.
1. The multiple paragraphs of text underneath, which are used to show the interaction of floating elements with text.
1. Finally the controls, in the gray zone at the bottom which will show you and let you change if a balloon is floating or not.

### Initially
None of the balloons are floating. They are stacked on top of each others, with the paragraphs of text underneath. We will see in the next sections a series of example, based on this game to understand the properties of floats.