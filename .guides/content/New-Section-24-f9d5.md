```html
<div id="user-panel">
Hi there, <span class="username"> John Smith </span> | <a href="#profile"> Profile </a> | <a href="#logout"> Logout </a>
</div>
```

(You can also see it on the left along with the preview)

We have used a div to wrap the whole thing together and give it an id of "user-panel" so we can target it later.
And we have used a span around "John Smith" with a class "username" so we can apply a special styling for the user name.