## display:inline-block;

In order to confuse you even more, here is a mode that's called "inline-block" :)

Basically it behaves like an inline element, except that it applies all of the box model properties.