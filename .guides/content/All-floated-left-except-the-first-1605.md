This demonstrates the fact that floats don't go all the way up their containers if there are other block-level elements before them in the container, they will float under the last block-level element.

Here, the first balloon is on its own line at the top, and the other ones are in a line 2-3-4-5 underneath it.

If you click "float:none" on the second balloon using the controls at the bottom, you will see that now, the first and second balloons are at the top, stacked on each other and then follow the paragraph of text, with the 3-4-5 balloons floating in a line. 

{Check It!|assessment}(test-2748978202)
