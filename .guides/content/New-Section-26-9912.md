### 2) Paragraph and emphasize example

```html
<div id="paragraph">
    This is <span class="emphasize">nice</span>
</div>
```

If you want a paragraph, use a `p` and if you want to emphasize something, use `em` or `b`, like this :

```html
<p>
    This is <b>nice</b>
</p>
```

([Click here](close_all; open_file divsandspans/dont-abuse-p.html panel=0; open_preview divsandspans/dont-abuse-p.html panel=1) to see this example on the left)


Earlier we have said that `<div>` is a block level element and that `<span>` is an inline element, let's head to the next section to see more about that.