### display : block

Here are some examples of elements who have `display:block` by default, they are called block-level elements : `<div>`, `<p>`, `<table>`, `<ul>`, `<ol>`

The properties of a block-level elements are :

- It will take the whole width of its parent by default.
- It will begin on a new line by default.
- It may contain other block-level elements as well as inline elements.